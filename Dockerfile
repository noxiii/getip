FROM golang:1.14.4-alpine3.12 AS builder
RUN apk update && apk add --no-cache git
WORKDIR $GOPATH/src/mypackage/getip/
COPY ./getip.go .
ENV CGO_ENABLED=0
RUN go get -d -v
RUN GOOS=linux GOARCH=amd64 go build -ldflags="-w -s" -o /go/bin/getip

FROM scratch
COPY --from=builder /go/bin/getip /go/bin/getip
ENTRYPOINT ["/go/bin/getip"]
