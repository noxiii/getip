package main

import (
   "fmt"
   "net/http"
   "net"
 )

var ipx, iph, ip string
func Index(w http.ResponseWriter, r *http.Request) {

   ipx = r.Header.Get("X-FORWARDED-FOR")
   iph,_,_ = net.SplitHostPort(r.RemoteAddr)

   if ipx != "" {
     ip = ipx
   } else if iph != "" {
     ip = iph
   } else {
     ip = "No IP detected"
   }

   fmt.Fprintf(w,ip)
}

func main() {
   http.HandleFunc("/", Index)
   http.ListenAndServe(":8080", nil)
}
