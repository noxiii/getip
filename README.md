# GetIP Docker webservice
## Description
Small Golang Docker Webservice to return the IP of client.
This Repo is only for testing

## Start 
Activate ip forwarding on Docker Host
sysctl -w net.ipv4.ip_forward=1
or push into /usr/lib/sysctl.d/docker-ip-forwarding.conf
net.ipv4.ip_forward = 1

### start container
docker start -p 8080:8080 getip

## Build
docker build .
